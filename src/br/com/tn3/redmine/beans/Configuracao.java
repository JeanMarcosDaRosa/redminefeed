/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tn3.redmine.beans;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author jean.marcos
 */
public class Configuracao {

    private Long timeSleep;
    private String urlMain;
    private String redmineKey;
    private String atom;
    private Long ultimoFeed;
    private String imagemBgd;
    //private Color cor = new Color(240, 240, 240);

    private boolean showissues = false;
    private boolean showchangesets = false;
    private boolean shownews = false;
    private boolean showdocuments = false;
    private boolean showfiles = false;
    private boolean showwikiedits = false;
    private boolean showmessages = false;
    private boolean showtimeentries = false;

    private boolean usaIntHideNotif = false;
    private Integer frameX = 350;
    private Integer frameY = 135;

    /**
     * @return the timeSleep
     */
    public Long getTimeSleep() {
        return timeSleep;
    }

    /**
     * @param timeSleep the timeSleep to set
     */
    public void setTimeSleep(Long timeSleep) {
        this.timeSleep = timeSleep;
    }

    /**
     * @return the urlMain
     */
    public String getUrlMain() {
        return urlMain;
    }

    /**
     * @param urlMain the urlMain to set
     */
    public void setUrlMain(String urlMain) {
        this.urlMain = urlMain;
    }

    /**
     * @return the redmineKey
     */
    public String getRedmineKey() {
        return redmineKey;
    }

    /**
     * @param redmineKey the redmineKey to set
     */
    public void setRedmineKey(String redmineKey) {
        this.redmineKey = redmineKey;
    }

    /**
     * @return the atom
     */
    public String getAtom() {
        return atom;
    }

    /**
     * @param atom the atom to set
     */
    public void setAtom(String atom) {
        this.atom = atom;
    }

    public Configuracao clonar() throws CloneNotSupportedException {
        Configuracao _return = new Configuracao();
        _return.setUrlMain(urlMain);
        _return.setAtom(atom);
        _return.setRedmineKey(redmineKey);
        _return.setTimeSleep(timeSleep);
        _return.setUltimoFeed(getUltimoFeed());
        //_return.setCor(cor);

        _return.setShowchangesets(showchangesets);
        _return.setShowdocuments(showdocuments);
        _return.setShowfiles(showfiles);
        _return.setShowissues(showissues);
        _return.setShowmessages(showmessages);
        _return.setShownews(shownews);
        _return.setImagemBgd(imagemBgd);
        _return.setShowtimeentries(showtimeentries);
        _return.setShowwikiedits(showwikiedits);

        _return.setUsaIntHideNotif(usaIntHideNotif);
        _return.setFrameX(frameX);
        _return.setFrameY(frameY);
        return _return;
    }

//    /**
//     * @return the cor
//     */
//    public Color getCor() {
//        return cor;
//    }
//
//    public String getCorStr() {
//        String res = "DEFAULT";
//        if (cor.equals(Color.WHITE)) {
//            res = "WHITE";
//        } else if (cor.equals(Color.LIGHT_GRAY)) {
//            res = "LIGHT_GRAY";
//        } else if (cor.equals(Color.GRAY)) {
//            res = "GRAY";
//        } else if (cor.equals(Color.DARK_GRAY)) {
//            res = "DARK_GRAY";
//        } else if (cor.equals(Color.BLACK)) {
//            res = "BLACK";
//        } else if (cor.equals(Color.RED)) {
//            res = "RED";
//        } else if (cor.equals(Color.PINK)) {
//            res = "PINK";
//        } else if (cor.equals(Color.ORANGE)) {
//            res = "ORANGE";
//        } else if (cor.equals(Color.YELLOW)) {
//            res = "YELLOW";
//        } else if (cor.equals(Color.GREEN)) {
//            res = "GREEN";
//        } else if (cor.equals(Color.MAGENTA)) {
//            res = "MAGENTA";
//        } else if (cor.equals(Color.CYAN)) {
//            res = "CYAN";
//        } else if (cor.equals(Color.BLUE)) {
//            res = "BLUE";
//        }
//        return res;
//    }
//
//    public String getCorRGBStr() {
//        return cor.getRed() + "," + cor.getGreen() + "," + cor.getBlue();
//    }
//
//    /**
//     * @param cor the cor to set
//     */
//    public void setCor(Color cor) {
//        this.cor = cor;
//    }
//
//    public void setCor(String[] cores) {
//        if (cores.length == 3) {
//            this.cor = (new Color(Integer.parseInt(cores[0]), Integer.parseInt(cores[1]), Integer.parseInt(cores[2])));
//        }
//    }

    private void setDimensionFrame(String[] xy) {
        if (xy.length == 2) {
            this.frameX = Integer.parseInt(xy[0]);
            this.frameY = Integer.parseInt(xy[1]);
            if(this.frameX<=0 || this.frameY<=0){
                System.out.println("Dimensões para o frame inválidas ["+this.frameX+","+this.frameY+"]");
                this.frameX = 350;
                this.frameY = 135;
            }
        }else{
            System.out.println("Dimensões para o frame inválidas");
        }
    }

    public void setOpcoes(Boolean value) {
        this.setShowissues(value);
        this.setShowchangesets(value);
        this.setShownews(value);
        this.setShowdocuments(value);
        this.setShowfiles(value);
        this.setShowwikiedits(value);
        this.setShowmessages(value);
        this.setShowtimeentries(value);
    }

    public void setOpcoes(String[] opcoes) {
        setOpcoes(false);
        for (String opt : opcoes) {
            switch (opt) {
                case "show_issues":
                    this.setShowissues(true);
                    break;
                case "show_changesets":
                    this.setShowchangesets(true);
                    break;
                case "show_news":
                    this.setShownews(true);
                    break;
                case "show_documents":
                    this.setShowdocuments(true);
                    break;
                case "show_files":
                    this.setShowfiles(true);
                    break;
                case "show_wiki_edits":
                    this.setShowwikiedits(true);
                    break;
                case "show_messages":
                    this.setShowmessages(true);
                    break;
                case "show_time_entries":
                    this.setShowtimeentries(true);
                    break;
            }
        }
    }

//    public void setCor(String color) {
//        Color res = new Color(240, 240, 240);
//        switch (color) {
//            case "WHITE":
//                res = Color.WHITE;
//                break;
//            case "LIGHT_GRAY":
//                res = Color.LIGHT_GRAY;
//                break;
//            case "GRAY":
//                res = Color.GRAY;
//                break;
//            case "DARK_GRAY":
//                res = Color.DARK_GRAY;
//                break;
//            case "BLACK":
//                res = Color.BLACK;
//                break;
//            case "RED":
//                res = Color.RED;
//                break;
//            case "PINK":
//                res = Color.PINK;
//                break;
//            case "ORANGE":
//                res = Color.ORANGE;
//                break;
//            case "YELLOW":
//                res = Color.YELLOW;
//                break;
//            case "GREEN":
//                res = Color.GREEN;
//                break;
//            case "MAGENTA":
//                res = Color.MAGENTA;
//                break;
//            case "CYAN":
//                res = Color.CYAN;
//                break;
//            case "BLUE":
//                res = Color.BLUE;
//                break;
//        }
//        this.cor = res;
//    }

    public String getOpcoes() {
        String _return = "";
        if (showchangesets) {
            _return += ",show_changesets";
        }
        if (showdocuments) {
            _return += ",show_documents";
        }
        if (showfiles) {
            _return += ",show_files";
        }
        if (showissues) {
            _return += ",show_issues";
        }
        if (showmessages) {
            _return += ",show_messages";
        }
        if (shownews) {
            _return += ",show_news";
        }
        if (showtimeentries) {
            _return += ",show_time_entries";
        }
        if (showwikiedits) {
            _return += ",show_wiki_edits";
        }
        if (!_return.equals("") && _return.charAt(0) == ',') {
            _return = _return.substring(1);
        }
        return _return;
    }

    public String getOpcoesLink() {
        String _return = "";
        if (showchangesets) {
            _return += "&show_changesets=1";
        }
        if (showdocuments) {
            _return += "&show_documents=1";
        }
        if (showfiles) {
            _return += "&show_files=1";
        }
        if (showissues) {
            _return += "&show_issues=1";
        }
        if (showmessages) {
            _return += "&show_messages=1";
        }
        if (shownews) {
            _return += "&show_news=1";
        }
        if (showtimeentries) {
            _return += "&show_time_entries=1";
        }
        if (showwikiedits) {
            _return += "&show_wiki_edits=1";
        }
        return _return;
    }

    public static Configuracao openConfiguracao(String filepath) {
        Configuracao configuracao = null;
        try (FileInputStream input = new FileInputStream(filepath)) {
            SAXBuilder builder = new SAXBuilder();
            Document doc = builder.build(input);
            // pega o root
            Element eleRoot = doc.getRootElement();
            // pega a lista do root
            List<Element> listFilhosRoot = eleRoot.getChildren();
            if (listFilhosRoot != null && listFilhosRoot.size() > 0) {
                for (Element eleConfiguracao : listFilhosRoot) {
                    if (eleConfiguracao.getName().equals("configuracao")) {
                        configuracao = new Configuracao();
                        List<Element> listFilhosConfig = eleConfiguracao.getChildren();
                        for (Element elePropriedade : listFilhosConfig) {
                            switch (elePropriedade.getAttributeValue("key")) {
                                case "feedkey":
                                    configuracao.setRedmineKey(elePropriedade.getValue());
                                    break;
                                case "sleep":
                                    configuracao.setTimeSleep(Long.parseLong(elePropriedade.getValue()));
                                    break;
                                case "url":
                                    configuracao.setUrlMain(elePropriedade.getValue());
                                    break;
                                case "atom":
                                    configuracao.setAtom(elePropriedade.getValue());
                                    break;
                                case "imagemBgd":
                                    configuracao.setImagemBgd(elePropriedade.getValue());
                                    break;
                                case "ultimo":
                                    try {
                                        configuracao.setUltimoFeed(Long.parseLong(elePropriedade.getValue()));
                                    } catch (Exception e) {
                                        System.out.println("Não encontrou a data do ultimo feed recebido");
                                    }
                                    break;
//                                case "cor":
//                                    if (elePropriedade.getValue().split(",").length == 3) {
//                                        configuracao.setCor(elePropriedade.getValue().split(","));
//                                    }
//                                    break;
                                case "opcoes":
                                    if (elePropriedade.getValue().split(",").length > 0) {
                                        configuracao.setOpcoes(elePropriedade.getValue().split(","));
                                    }
                                    break;
                                case "hide_notificacao":
                                    configuracao.setUsaIntHideNotif(isTrue(elePropriedade.getValue()));
                                    break;
                                case "frameXY":
                                    if (elePropriedade.getValue().split(",").length == 2) {
                                        configuracao.setDimensionFrame(elePropriedade.getValue().split(","));
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
        } catch (JDOMException | IOException e) {
            System.out.println("Erro ao Carregar Configuracoes do Arquivo! \nErro:" + e.getMessage());
            System.exit(0);
        }
        return configuracao;
    }

    public static boolean updateConfig(Configuracao newConfig) {
        boolean bRetorno;
        try {
            FileInputStream input = new FileInputStream(System.getProperty("user.dir") + "/conf/configuracao.xml");
            SAXBuilder builder = new SAXBuilder();
            Document doc = builder.build(input);
            input.close();
            Element eleRoot = doc.getRootElement();
            Element eleRootNovo = (Element) eleRoot.clone();
            eleRootNovo.removeContent();
            Element eleConfNova = new Element("configuracao");

            Element eleKey = new Element("propriedade");
            eleKey.setAttribute("key", "feedkey");
            eleKey.setText(newConfig.getRedmineKey());
            eleConfNova.addContent(eleKey);

            Element eleSleep = new Element("propriedade");
            eleSleep.setAttribute("key", "sleep");
            eleSleep.setText(String.valueOf(newConfig.getTimeSleep()));
            eleConfNova.addContent(eleSleep);

            Element eleUrl = new Element("propriedade");
            eleUrl.setAttribute("key", "url");
            eleUrl.setText(String.valueOf(newConfig.getUrlMain()));
            eleConfNova.addContent(eleUrl);
            
            Element eleBgd = new Element("propriedade");
            eleBgd.setAttribute("key", "imagemBgd");
            eleBgd.setText(String.valueOf(newConfig.getImagemBgd()));
            eleConfNova.addContent(eleBgd);

            Element eleAtom = new Element("propriedade");
            eleAtom.setAttribute("key", "atom");
            eleAtom.setText(newConfig.getAtom());
            eleConfNova.addContent(eleAtom);

            Element eleUltimo = new Element("propriedade");
            eleUltimo.setAttribute("key", "ultimo");
            eleUltimo.setText(String.valueOf(newConfig.getUltimoFeed()));
            eleConfNova.addContent(eleUltimo);

//            Element eleCor = new Element("propriedade");
//            eleCor.setAttribute("key", "cor");
//            eleCor.setText(newConfig.getCorRGBStr());
//            eleConfNova.addContent(eleCor);

            Element eleOpcoes = new Element("propriedade");
            eleOpcoes.setAttribute("key", "opcoes");
            eleOpcoes.setText(newConfig.getOpcoes());
            eleConfNova.addContent(eleOpcoes);
            
            Element eleHide = new Element("propriedade");
            eleHide.setAttribute("key", "hide_notificacao");
            eleHide.setText(String.valueOf(newConfig.isUsaIntHideNotif()));
            eleConfNova.addContent(eleHide);
            
            Element eleFrame = new Element("propriedade");
            eleFrame.setAttribute("key", "frameXY");
            eleFrame.setText(newConfig.getFrameX()+","+newConfig.getFrameY());
            eleConfNova.addContent(eleFrame);

            eleRootNovo.addContent(eleConfNova);

            // salva no xml
            FileOutputStream out = new FileOutputStream(System.getProperty("user.dir") + "/conf/configuracao.xml");
            Document docNovo = new Document();
            docNovo.setRootElement(eleRootNovo);
            XMLOutputter outXml = new XMLOutputter(Format.getPrettyFormat());
            outXml.output(docNovo, out);
            out.close();
            bRetorno = true;
            System.out.println("Configuracao Alterada com Sucesso!");
        } catch (JDOMException | IOException e) {
            bRetorno = false;
            //System.out.println("Erro no updateFilialArquivo!!");
            System.out.println("Erro ao alterar configuracao\nErro:" + e.getMessage());
        }
        return bRetorno;
    }

    private static boolean isTrue(String value) {
        return (value != null && (value.equals("1") || value.equalsIgnoreCase("true")));
    }

    /**
     * @return the showissues
     */
    public boolean isShowissues() {
        return showissues;
    }

    /**
     * @param showissues the showissues to set
     */
    public void setShowissues(boolean showissues) {
        this.showissues = showissues;
    }

    /**
     * @return the showchangesets
     */
    public boolean isShowchangesets() {
        return showchangesets;
    }

    /**
     * @param showchangesets the showchangesets to set
     */
    public void setShowchangesets(boolean showchangesets) {
        this.showchangesets = showchangesets;
    }

    /**
     * @return the shownews
     */
    public boolean isShownews() {
        return shownews;
    }

    /**
     * @param shownews the shownews to set
     */
    public void setShownews(boolean shownews) {
        this.shownews = shownews;
    }

    /**
     * @return the showdocuments
     */
    public boolean isShowdocuments() {
        return showdocuments;
    }

    /**
     * @param showdocuments the showdocuments to set
     */
    public void setShowdocuments(boolean showdocuments) {
        this.showdocuments = showdocuments;
    }

    /**
     * @return the showfiles
     */
    public boolean isShowfiles() {
        return showfiles;
    }

    /**
     * @param showfiles the showfiles to set
     */
    public void setShowfiles(boolean showfiles) {
        this.showfiles = showfiles;
    }

    /**
     * @return the showwikiedits
     */
    public boolean isShowwikiedits() {
        return showwikiedits;
    }

    /**
     * @param showwikiedits the showwikiedits to set
     */
    public void setShowwikiedits(boolean showwikiedits) {
        this.showwikiedits = showwikiedits;
    }

    /**
     * @return the showmessages
     */
    public boolean isShowmessages() {
        return showmessages;
    }

    /**
     * @param showmessages the showmessages to set
     */
    public void setShowmessages(boolean showmessages) {
        this.showmessages = showmessages;
    }

    /**
     * @return the showtimeentries
     */
    public boolean isShowtimeentries() {
        return showtimeentries;
    }

    /**
     * @param showtimeentries the showtimeentries to set
     */
    public void setShowtimeentries(boolean showtimeentries) {
        this.showtimeentries = showtimeentries;
    }

    /**
     * @return the ultimoFeed
     */
    public Long getUltimoFeed() {
        return ultimoFeed;
    }

    /**
     * @param ultimoFeed the ultimoFeed to set
     */
    public void setUltimoFeed(Long ultimoFeed) {
        this.ultimoFeed = ultimoFeed;
    }

    /**
     * @return the usaIntHideNotif
     */
    public boolean isUsaIntHideNotif() {
        return usaIntHideNotif;
    }

    /**
     * @param usaIntHideNotif the usaIntHideNotif to set
     */
    public void setUsaIntHideNotif(boolean usaIntHideNotif) {
        this.usaIntHideNotif = usaIntHideNotif;
    }

    /**
     * @return the frameX
     */
    public Integer getFrameX() {
        return frameX;
    }

    /**
     * @param frameX the frameX to set
     */
    public void setFrameX(Integer frameX) {
        this.frameX = frameX;
    }

    /**
     * @return the frameY
     */
    public Integer getFrameY() {
        return frameY;
    }

    /**
     * @param frameY the frameY to set
     */
    public void setFrameY(Integer frameY) {
        this.frameY = frameY;
    }

    /**
     * @return the imagemBgd
     */
    public String getImagemBgd() {
        return imagemBgd;
    }

    /**
     * @param imagemBgd the imagemBgd to set
     */
    public void setImagemBgd(String imagemBgd) {
        this.imagemBgd = imagemBgd;
    }

}
