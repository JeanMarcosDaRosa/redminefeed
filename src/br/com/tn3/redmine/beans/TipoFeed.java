/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tn3.redmine.beans;

import java.util.Objects;
import javax.swing.ImageIcon;

/**
 *
 * @author jean.marcos
 */
public class TipoFeed {
    private String nome;
    private ImageIcon icone;

    public TipoFeed(String nome, ImageIcon icone) {
        this.nome = nome;
        this.icone = icone;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.nome);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoFeed other = (TipoFeed) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        return true;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the icone
     */
    public ImageIcon getIcone() {
        return icone;
    }

    /**
     * @param icone the icone to set
     */
    public void setIcone(ImageIcon icone) {
        this.icone = icone;
    }
}
