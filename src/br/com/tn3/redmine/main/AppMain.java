/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tn3.redmine.main;

import br.com.tn3.redmine.beans.Configuracao;
import br.com.tn3.redmine.beans.TipoFeed;
import br.com.tn3.redmine.gui.ConfigWindow;
import br.com.tn3.redmine.gui.Notificacao;
import br.com.tn3.redmine.http.Conector;
import br.com.tn3.redmine.feedparser.Feed;
import br.com.tn3.redmine.feedparser.Item;
import br.com.tn3.redmine.feedparser.impl.DefaultFeedParser;
import java.awt.AWTException;
import java.awt.Desktop;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author jean.marcos
 */
public class AppMain implements ActionListener {

    private static PopupMenu popupMenu;
    private static MenuItem mnuSair, mnuConfig, mnuAtualizar, mnuShowRedmine, mnuPauseStart;
    private static Menu historico;
    private static AppMain _instance;
    private static boolean PAUSA = false;
    private Configuracao conf = Configuracao.openConfiguracao(System.getProperty("user.dir") + "/conf/configuracao.xml");
    ConfigWindow configWindow = new ConfigWindow();
    List<TipoFeed> tipos = getListaTipos();
    DefaultFeedParser feedParser = new DefaultFeedParser();
    Conector http = new Conector();
    List<Item> items = new ArrayList<>();

    public static AppMain getInstance() {
        return _instance;
    }

    static {
        try {
            _instance = new AppMain();
        } catch (Exception e) {
        }
    }

    public static void main(String[] args) {
        AppMain.getInstance().initApp();

    }

    public static String getDataFormatada(Date data) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return formatter.format(data);
    }

    public static Date getDataFormatada(String data) {
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date _return = (Date) formatter.parse(data);
            return _return;
        } catch (Exception ex) {
            return null;
        }
    }

    public static Date getData(Long data) {
        try {
            Date _return = new Date(data);
            return _return;
        } catch (Exception ex) {
            return null;
        }
    }

    private void criaMenus() {
        popupMenu = new PopupMenu();
        mnuAtualizar = new MenuItem("Atualizar Feeds");
        mnuPauseStart = new MenuItem("Pausar Feeds");
        mnuShowRedmine = new MenuItem("Abrir no Redmine");
        mnuConfig = new MenuItem("Configurações");
        historico = new Menu("Histórico");
        mnuSair = new MenuItem("Sair");
        popupMenu.add(mnuAtualizar);
        popupMenu.add(mnuPauseStart);
        popupMenu.addSeparator();
        popupMenu.add(mnuShowRedmine);
        popupMenu.add(mnuConfig);
        popupMenu.add(historico);
        popupMenu.addSeparator();
        popupMenu.add(mnuSair);
        mnuAtualizar.addActionListener(this);
        mnuPauseStart.addActionListener(this);
        mnuShowRedmine.addActionListener(this);
        mnuConfig.addActionListener(this);
        mnuSair.addActionListener(this);
        SystemTray tray = SystemTray.getSystemTray();
        if (SystemTray.isSupported()) {
            TrayIcon trayIcon = new TrayIcon(new ImageIcon(Notificacao.class.getResource("tray.gif")).getImage(), "Redmine Feeds", popupMenu);
            try {
                tray.add(trayIcon);
            } catch (AWTException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.exit(0);
        }
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (evt.getSource() == mnuSair) {
            if (JOptionPane.showConfirmDialog(null, "Deseja realmente sair?") == 0) {
                System.exit(0);
            }
        } else if (evt.getSource() == mnuConfig) {
            configWindow.setConfiguracao(getConf());
            configWindow.editar(false);
            configWindow.setVisible(true);
        } else if (evt.getSource() == mnuShowRedmine) {
            if (Desktop.isDesktopSupported()) {
                Desktop desktop = Desktop.getDesktop();
                try {
                    String act = getConf().getUrlMain().endsWith("/") ? "activity" : "/activity";
                    URI uri = new URI(getConf().getUrlMain() + act);
                    desktop.browse(uri);
                } catch (IOException | URISyntaxException ex) {
                }
            }
        } else if (evt.getSource() == mnuAtualizar) {
            try {
                consultarFeeds(feedParser, http, items);
            } catch (Exception ex) {
                Logger.getLogger(AppMain.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (evt.getSource() == mnuPauseStart) {
            PAUSA = !PAUSA;
            if (PAUSA) {
                mnuPauseStart.setLabel("Continuar Feeds");
            } else {
                mnuPauseStart.setLabel("Pausar Feeds");
                try {
                    consultarFeeds(feedParser, http, items);
                } catch (Exception ex) {
                    Logger.getLogger(AppMain.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void initApp() {
        feedParser = new DefaultFeedParser();
        http = new Conector();
        items = new ArrayList<>();
        try {
            criaMenus();
            while (true) {
                //Busca o conteudo do atom
                if (!PAUSA) {
                    consultarFeeds(feedParser, http, items);
                }
                Thread.sleep(getConf().getTimeSleep());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @return the conf
     */
    public Configuracao getConf() {
        return conf;
    }

    /**
     * @param conf the conf to set
     */
    public void setConf(Configuracao conf) {
        this.conf = conf;
    }

    private TipoFeed getTipoByFeed(Item s, List<TipoFeed> lista) {
        for (TipoFeed t : lista) {
            String titulo = s.getTitle();
            if (s.getTitle().indexOf(":") > 0) {
                titulo = titulo.substring(0, s.getTitle().indexOf(":"));
            }
            if (titulo.contains(t.getNome())) {
                return t;
            }
        }
        return new TipoFeed("outro", new ImageIcon(Notificacao.class.getResource("outros.png")));
    }

    private List<TipoFeed> getListaTipos() {
        List<TipoFeed> lista = new ArrayList<>();
        lista.add(new TipoFeed("Bug", new ImageIcon(Notificacao.class.getResource("bug.png"))));
        lista.add(new TipoFeed("Customização", new ImageIcon(Notificacao.class.getResource("customizacao.png"))));
        lista.add(new TipoFeed("Desenvolvimento", new ImageIcon(Notificacao.class.getResource("desenvolvimento.png"))));
        lista.add(new TipoFeed("Migração Versão / Atualização", new ImageIcon(Notificacao.class.getResource("atualizacao.png"))));
        lista.add(new TipoFeed("Revisão", new ImageIcon(Notificacao.class.getResource("revisao.png"))));
        lista.add(new TipoFeed("Suporte", new ImageIcon(Notificacao.class.getResource("suporte.png"))));
        lista.add(new TipoFeed("Treinamento", new ImageIcon(Notificacao.class.getResource("treinamento.png"))));
        return lista;
    }

    private void consultarFeeds(DefaultFeedParser feedParser, Conector http, List<Item> items) throws Exception {
        String urlAtom = getConf().getUrlMain() + "/" + getConf().getAtom() + "?key=" + getConf().getRedmineKey() + getConf().getOpcoesLink();
        String atom = http.GetPageContent(urlAtom, false);
        InputStream inStream = new ByteArrayInputStream(atom.getBytes());//StandardCharsets.ISO_8859_1
        Feed feed = feedParser.parse(inStream);
        Date ultimoFeed = getData(conf.getUltimoFeed());
        Date max = getData(conf.getUltimoFeed());
        Boolean first = (items.isEmpty());
        for (Item s : feed.getItemList()) {
            if (!items.contains(s)) {
                items.add(s);
                System.out.println(s.getPubDate() + " - " + s);
                TipoFeed tipo = getTipoByFeed(s, tipos);
                String header;
                if (s.getAuthor() == null) {
                    header = "Documentação" + "<br/>" + getDataFormatada(s.getPubDate());
                } else {
                    header = s.getAuthor() + "<br/>" + getDataFormatada(s.getPubDate());
                }
                final Notificacao n = new Notificacao(header, "<p>" + s.getTitle() + "</p>", s.getLink(), tipo, getConf());
                if (s.getDescription() != null) {
                    if (historico.getItemCount() >= 15) {
                        historico.remove(0);
                    }
                    MenuItem mm = new MenuItem(s.getTitle());
                    mm.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (e.getSource() instanceof MenuItem) {
                                MenuItem it = (MenuItem) e.getSource();
                                Menu parent = ((Menu) it.getParent());
                                if (parent == historico) {
                                    n.show(conf);
                                }
                            }
                        }
                    });
                    if (first) {
                        historico.insert(mm, 0);
                    } else {
                        historico.add(mm);
                    }
                }
                if (ultimoFeed == null || s.getPubDate().after(ultimoFeed)) {
                    n.show(conf);
                    if (max == null || s.getPubDate().after(max)) {
                        max = s.getPubDate();
                    }
                } else {
                    n.setSleep(0L);
                }
            }
        }
        if (ultimoFeed == null || !getDataFormatada(max).equals(getDataFormatada(ultimoFeed))) {
            conf.setUltimoFeed(max.getTime());
            Configuracao.updateConfig(conf);
        }
    }

}
