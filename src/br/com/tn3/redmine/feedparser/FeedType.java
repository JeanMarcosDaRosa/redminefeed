package br.com.tn3.redmine.feedparser;

/**
 * Supported feed types.
 */
public enum FeedType {
	RSS_1_0, RSS_2_0, ATOM_1_0
}