/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tn3.redmine.gui;

import br.com.tn3.redmine.main.AppMain;
import br.com.tn3.redmine.beans.Configuracao;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author jean.marcos
 */
public class ConfigWindow extends javax.swing.JFrame {

    private Configuracao configuracao, bkpConfiguracao;
    JFileChooser fileChooser = new JFileChooser();

    /**
     * Creates new form ConfigWindow
     */
    public ConfigWindow() {
        initComponents();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPnlPrincipal = new javax.swing.JPanel();
        jPnlSup = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPnlConf = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        edtHost = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        edtAtom = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        edtKey = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        edtIntervalo = new javax.swing.JSpinner();
        jPanel2 = new javax.swing.JPanel();
        show_files = new javax.swing.JCheckBox();
        show_messages = new javax.swing.JCheckBox();
        show_changesets = new javax.swing.JCheckBox();
        show_time_entries = new javax.swing.JCheckBox();
        show_news = new javax.swing.JCheckBox();
        show_issues = new javax.swing.JCheckBox();
        show_documents = new javax.swing.JCheckBox();
        show_wiki_edits = new javax.swing.JCheckBox();
        cbUsaIntervalo = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        edtNotificacaoX = new javax.swing.JSpinner();
        jLabel8 = new javax.swing.JLabel();
        edtNotificacaoY = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        cbImagemDefault = new javax.swing.JCheckBox();
        edtImagem = new javax.swing.JTextField();
        btnAbrirImagem = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnSalvar = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnCanc = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPnlSup.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel1.setText("Configurações Gerais");

        javax.swing.GroupLayout jPnlSupLayout = new javax.swing.GroupLayout(jPnlSup);
        jPnlSup.setLayout(jPnlSupLayout);
        jPnlSupLayout.setHorizontalGroup(
            jPnlSupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlSupLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPnlSupLayout.setVerticalGroup(
            jPnlSupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlSupLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jPnlConf.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel2.setText("Host");

        edtHost.setEnabled(false);

        jLabel3.setText("Atom");

        edtAtom.setEnabled(false);

        jLabel4.setText("Key");

        edtKey.setEnabled(false);

        jLabel5.setText("Intervalo");

        edtIntervalo.setEnabled(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Opções de Busca"));

        show_files.setText("Arquivos");
        show_files.setEnabled(false);

        show_messages.setText("Mensagens");
        show_messages.setEnabled(false);

        show_changesets.setText("Changesets");
        show_changesets.setEnabled(false);

        show_time_entries.setText("Tempos gastos");
        show_time_entries.setEnabled(false);

        show_news.setText("Notícias");
        show_news.setEnabled(false);

        show_issues.setText("Tarefas");
        show_issues.setEnabled(false);

        show_documents.setText("Documentos");
        show_documents.setEnabled(false);

        show_wiki_edits.setText("Edições Wiki");
        show_wiki_edits.setEnabled(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(show_issues)
                    .addComponent(show_changesets)
                    .addComponent(show_news)
                    .addComponent(show_documents))
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(show_files)
                    .addComponent(show_wiki_edits)
                    .addComponent(show_messages)
                    .addComponent(show_time_entries))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(show_issues)
                    .addComponent(show_files))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(show_changesets)
                    .addComponent(show_wiki_edits))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(show_news)
                    .addComponent(show_messages))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(show_documents)
                    .addComponent(show_time_entries))
                .addContainerGap())
        );

        cbUsaIntervalo.setText("Ocultar a janela de notivicação");
        cbUsaIntervalo.setActionCommand("Usar o intervalo para ocultar a janela de notificação");
        cbUsaIntervalo.setEnabled(false);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Dimenções da janela de notificação"));

        jLabel7.setText("X:");

        edtNotificacaoX.setEnabled(false);

        jLabel8.setText("Y:");

        edtNotificacaoY.setEnabled(false);

        jLabel6.setText("Imagem:");

        cbImagemDefault.setText("Plano de Fundo padrão");
        cbImagemDefault.setEnabled(false);
        cbImagemDefault.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                cbImagemDefaultStateChanged(evt);
            }
        });

        edtImagem.setEnabled(false);

        btnAbrirImagem.setText("...");
        btnAbrirImagem.setEnabled(false);
        btnAbrirImagem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirImagemActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(cbImagemDefault, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtNotificacaoX, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtNotificacaoY, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(edtImagem)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAbrirImagem)))
                        .addContainerGap())))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {edtNotificacaoX, edtNotificacaoY});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(edtNotificacaoX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(edtNotificacaoY, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbImagemDefault)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(edtImagem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAbrirImagem))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPnlConfLayout = new javax.swing.GroupLayout(jPnlConf);
        jPnlConf.setLayout(jPnlConfLayout);
        jPnlConfLayout.setHorizontalGroup(
            jPnlConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlConfLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPnlConfLayout.createSequentialGroup()
                        .addGroup(jPnlConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPnlConfLayout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addGroup(jPnlConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel4)
                                    .addGroup(jPnlConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel2)
                                        .addComponent(jLabel3))))
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPnlConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(edtAtom, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(edtHost, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(edtKey)
                            .addGroup(jPnlConfLayout.createSequentialGroup()
                                .addComponent(edtIntervalo, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbUsaIntervalo)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPnlConfLayout.setVerticalGroup(
            jPnlConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlConfLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPnlConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(edtHost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPnlConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(edtAtom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPnlConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(edtKey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPnlConfLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(edtIntervalo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbUsaIntervalo)
                    .addComponent(jLabel5))
                .addGap(9, 9, 9)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnSalvar.setText("Salvar");
        btnSalvar.setEnabled(false);
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnAlterar.setText("Alterar");
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });

        btnCanc.setText("Cancelar");
        btnCanc.setEnabled(false);
        btnCanc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAlterar)
                .addGap(30, 30, 30)
                .addComponent(btnCanc)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalvar)
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAlterar, btnCanc, btnSalvar});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalvar)
                    .addComponent(btnAlterar)
                    .addComponent(btnCanc))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPnlPrincipalLayout = new javax.swing.GroupLayout(jPnlPrincipal);
        jPnlPrincipal.setLayout(jPnlPrincipalLayout);
        jPnlPrincipalLayout.setHorizontalGroup(
            jPnlPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlSup, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPnlConf, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPnlPrincipalLayout.setVerticalGroup(
            jPnlPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPnlPrincipalLayout.createSequentialGroup()
                .addComponent(jPnlSup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPnlConf, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        try {
            bkpConfiguracao = configuracao.clonar();
            editar(true);
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(ConfigWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnCancActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancActionPerformed
        try {
            setConfiguracao(bkpConfiguracao.clonar());
            editar(false);
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(ConfigWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnCancActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        salvar();
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnAbrirImagemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirImagemActionPerformed
        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            edtImagem.setText(selectedFile.getAbsolutePath());
        }
    }//GEN-LAST:event_btnAbrirImagemActionPerformed

    private void cbImagemDefaultStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_cbImagemDefaultStateChanged
        edtImagem.setEnabled(!cbImagemDefault.isSelected());
        btnAbrirImagem.setEnabled(!cbImagemDefault.isSelected());
    }//GEN-LAST:event_cbImagemDefaultStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConfigWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConfigWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConfigWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConfigWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ConfigWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbrirImagem;
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnCanc;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JCheckBox cbImagemDefault;
    private javax.swing.JCheckBox cbUsaIntervalo;
    private javax.swing.JTextField edtAtom;
    private javax.swing.JTextField edtHost;
    private javax.swing.JTextField edtImagem;
    private javax.swing.JSpinner edtIntervalo;
    private javax.swing.JTextField edtKey;
    private javax.swing.JSpinner edtNotificacaoX;
    private javax.swing.JSpinner edtNotificacaoY;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPnlConf;
    private javax.swing.JPanel jPnlPrincipal;
    private javax.swing.JPanel jPnlSup;
    private javax.swing.JCheckBox show_changesets;
    private javax.swing.JCheckBox show_documents;
    private javax.swing.JCheckBox show_files;
    private javax.swing.JCheckBox show_issues;
    private javax.swing.JCheckBox show_messages;
    private javax.swing.JCheckBox show_news;
    private javax.swing.JCheckBox show_time_entries;
    private javax.swing.JCheckBox show_wiki_edits;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the configuracao
     */
    public Configuracao getConfiguracao() {
        return configuracao;
    }

    /**
     * @param configuracao the configuracao to set
     */
    public void setConfiguracao(Configuracao configuracao) {
        this.configuracao = configuracao;
        edtHost.setText(this.configuracao.getUrlMain());
        edtAtom.setText(this.configuracao.getAtom());
        edtKey.setText(this.configuracao.getRedmineKey());
        edtIntervalo.setValue(this.configuracao.getTimeSleep() != null && this.configuracao.getTimeSleep() > 0L ? (this.configuracao.getTimeSleep() / 1000) : 0L);
        edtImagem.setText(this.configuracao.getImagemBgd());
        cbImagemDefault.setSelected(edtImagem.getText().equals(""));
        //edtCor.setSelectedItem(configuracao.getCorStr());
        show_changesets.setSelected(this.configuracao.isShowchangesets());
        show_documents.setSelected(this.configuracao.isShowdocuments());
        show_files.setSelected(this.configuracao.isShowfiles());
        show_issues.setSelected(this.configuracao.isShowissues());
        show_messages.setSelected(this.configuracao.isShowmessages());
        show_news.setSelected(this.configuracao.isShownews());
        show_time_entries.setSelected(this.configuracao.isShowtimeentries());
        show_wiki_edits.setSelected(this.configuracao.isShowwikiedits());
        cbUsaIntervalo.setSelected(this.configuracao.isUsaIntHideNotif());
        edtNotificacaoX.setValue(this.configuracao.getFrameX());
        edtNotificacaoY.setValue(this.configuracao.getFrameY());
    }

    public void editar(boolean value) {
        edtHost.setEnabled(value);
        edtAtom.setEnabled(value);
        edtKey.setEnabled(value);
        edtIntervalo.setEnabled(value);
        cbImagemDefault.setEnabled(value);
        edtImagem.setEnabled(value);
        btnAbrirImagem.setEnabled(value);
        //edtCor.setEnabled(value);
        show_changesets.setEnabled(value);
        show_documents.setEnabled(value);
        show_files.setEnabled(value);
        show_issues.setEnabled(value);
        show_messages.setEnabled(value);
        show_news.setEnabled(value);
        show_time_entries.setEnabled(value);
        show_wiki_edits.setEnabled(value);
        cbUsaIntervalo.setEnabled(value);
        edtNotificacaoX.setEnabled(value);
        edtNotificacaoY.setEnabled(value);
        btnCanc.setEnabled(value);
        btnSalvar.setEnabled(value);
        btnAlterar.setEnabled(!value);
    }

    private void salvar() {
        configuracao.setUrlMain(edtHost.getText().trim());
        configuracao.setAtom(edtAtom.getText().trim());
        configuracao.setRedmineKey(edtKey.getText().trim());
        configuracao.setTimeSleep(Long.valueOf(edtIntervalo.getValue().toString()) * 1000);
        configuracao.setImagemBgd(cbImagemDefault.isSelected() ? "" : edtImagem.getText());
        //configuracao.setCor(edtCor.getSelectedItem().toString());
        configuracao.setOpcoes(getOpcoes());
        configuracao.setUsaIntHideNotif(cbUsaIntervalo.isSelected());
        if (Integer.valueOf(edtNotificacaoX.getValue().toString()) <= 0 || Integer.valueOf(edtNotificacaoY.getValue().toString()) <= 0) {
            JOptionPane.showMessageDialog(this, "Dimensões para o frame inválidas [" + edtNotificacaoX.getValue().toString() + "," + edtNotificacaoX.getValue().toString() + "]");
            return;
        } else {
            configuracao.setFrameX(Integer.valueOf(edtNotificacaoX.getValue().toString()));
            configuracao.setFrameY(Integer.valueOf(edtNotificacaoY.getValue().toString()));
        }
        if (salvar(configuracao)) {
            JOptionPane.showMessageDialog(this, "Alterações salvas com sucesso!");
            AppMain.getInstance().setConf(configuracao);
            editar(false);
        } else {
            JOptionPane.showMessageDialog(this, "Falha ao salvar alterações!");
        }
    }

    private boolean salvar(Configuracao config) {
        return Configuracao.updateConfig(config);
    }

    public String[] getOpcoes() {
        String _return = "";
        if (show_changesets.isSelected()) {
            _return += ",show_changesets";
        }
        if (show_documents.isSelected()) {
            _return += ",show_documents";
        }
        if (show_files.isSelected()) {
            _return += ",show_files";
        }
        if (show_issues.isSelected()) {
            _return += ",show_issues";
        }
        if (show_messages.isSelected()) {
            _return += ",show_messages";
        }
        if (show_news.isSelected()) {
            _return += ",show_news";
        }
        if (show_time_entries.isSelected()) {
            _return += ",show_time_entries";
        }
        if (show_wiki_edits.isSelected()) {
            _return += ",show_wiki_edits";
        }
        if (!_return.equals("") && _return.charAt(0) == ',') {
            _return = _return.substring(1);
        }
        return _return.split(",");
    }

}
