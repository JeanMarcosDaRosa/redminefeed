/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tn3.redmine.gui;

import br.com.tn3.redmine.beans.Configuracao;
import br.com.tn3.redmine.beans.TipoFeed;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

/**
 *
 * @author jean.marcos
 */
public final class Notificacao implements Runnable {

    final JFrame frame = new JFrame();
    JLabel headingLabel = null;
    String header = "";
    private Thread t;
    private Long sleep;
    Configuracao config;

    public Notificacao(String header, String message, final String link, TipoFeed tipo, Configuracao config) throws InterruptedException {
        this.config = config;
        this.header = header;
        if (config.isUsaIntHideNotif()) {
            sleep = config.getTimeSleep();
        }
        frame.setSize(config.getFrameX(), config.getFrameY());
        frame.setUndecorated(true);
        if (config.getImagemBgd() != null && !config.getImagemBgd().equals("")) {
            frame.setContentPane(new JPanelImage(config.getImagemBgd()));
        } else {
            frame.setContentPane(new JPanelImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/br/com/tn3/redmine/gui/background.jpg"))));
        }
        frame.setLayout(new GridBagLayout());
        frame.setAlwaysOnTop(true);
        frame.setFocusable(false);
        frame.setAutoRequestFocus(false);
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 1.0f;
        constraints.weighty = 1.0f;
        constraints.insets = new Insets(5, 5, 5, 5);
        constraints.fill = GridBagConstraints.BOTH;
        headingLabel = new JLabel(header);
        headingLabel.setForeground(Color.WHITE);
        headingLabel.setIcon(tipo.getIcone());
        headingLabel.setOpaque(false);
        frame.add(headingLabel, constraints);

        constraints.gridx++;
        constraints.weightx = 0f;
        constraints.weighty = 0f;
        constraints.fill = GridBagConstraints.NONE;
        constraints.anchor = GridBagConstraints.NORTH;

        JButton closeButton = new JButton(new AbstractAction("x") {
            @Override
            public void actionPerformed(ActionEvent ae) {
                frame.dispose();
            }
        });

        closeButton.setMargin(new Insets(1, 4, 1, 4));
        closeButton.setFocusable(false);
        closeButton.setBorder(new RoundedBorder(7));
        frame.add(closeButton, constraints);

        constraints.gridx = 0;
        constraints.gridy++;
        constraints.weightx = 1.0f;
        constraints.weighty = 1.0f;
        constraints.insets = new Insets(5, 20, 5, 5);
        constraints.fill = GridBagConstraints.BOTH;

        Dimension scrSize = Toolkit.getDefaultToolkit().getScreenSize();// size of the screen
        Insets toolHeight = Toolkit.getDefaultToolkit().getScreenInsets(frame.getGraphicsConfiguration());// height of the task bar
        frame.setLocation(scrSize.width - frame.getWidth(), scrSize.height - toolHeight.bottom - frame.getHeight());

        JLabel messageLabel = new JLabel("<html>" + message + "</html>");
        messageLabel.setForeground(Color.WHITE);
        frame.add(messageLabel, constraints);

        JLabel linkLabel = new JLabel("<html><FONT color=\"#000099\"><U>Ver</U></FONT></html>");
        linkLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() > 0) {
                    if (Desktop.isDesktopSupported()) {
                        Desktop desktop = Desktop.getDesktop();
                        try {
                            URI uri = new URI(link);
                            desktop.browse(uri);
                        } catch (IOException | URISyntaxException ex) {
                        }
                    }
                }
            }
        });

        constraints.gridx = 0;
        constraints.gridy++;
        constraints.weightx = 1.0f;
        constraints.weighty = 1.0f;
        constraints.insets = new Insets(5, 20, 5, 5);
        constraints.fill = GridBagConstraints.BOTH;

        frame.add(linkLabel, constraints);
        frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);

    }

    public void show(Configuracao config) {
        t = new Thread(this);
        headingLabel.setText("<html>" + header + "</html>");
        //frame.getContentPane().setBackground(config.getCor());
        frame.setSize(config.getFrameX(), config.getFrameY());
        t.start();
    }

    @Override
    public void run() {
        try {
            frame.setVisible(true);
            if (getSleep() != null && getSleep() > 0L) {
                Thread.sleep(getSleep());
                setSleep((Long) 0L);
                frame.setVisible(false);
                frame.dispose();
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(Notificacao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the sleep
     */
    public Long getSleep() {
        return sleep;
    }

    /**
     * @param sleep the sleep to set
     */
    public void setSleep(Long sleep) {
        this.sleep = sleep;
    }

}
